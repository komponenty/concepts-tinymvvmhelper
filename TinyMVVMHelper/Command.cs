﻿using System;
using System.Windows.Input;

namespace TinyMVVMHelper
{
    public class Command : ICommand
    {
        private Action<object> action;
        private Predicate<object> canExecute;

        public event EventHandler CanExecuteChanged;
        public event EventHandler<CommandExecutionArgs> BeforeExecution;
        public event EventHandler<CommandExecutionArgs> AfterExecution;

        public Command(Action<object> action, Predicate<object> canExecute)
        {
            this.action = action;
            this.canExecute = canExecute;
        }

        public Command(Action<object> action, bool canExecute = true)
            : this(action, (param) => canExecute) { }

        public bool CanExecute(object parameter)
        {
            return canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            var eventArgs = new CommandExecutionArgs(parameter);
            if (BeforeExecution != null)
                BeforeExecution(this, eventArgs);
            if (action != null)
                action(parameter);
            if (AfterExecution != null)
                AfterExecution(this, eventArgs);
        }

        public Predicate<object> ExecutePredicate
        {
            get { return canExecute; }
            set
            {
                canExecute = value;
                if (CanExecuteChanged != null)
                    CanExecuteChanged(this, EventArgs.Empty);
            }
        }

        public Action<object> ExecuteAction
        {
            get { return action; }
            set { action = value; }
        }
    }

    public class CommandExecutionArgs : EventArgs
    {
        public object ExecutionParam { get; private set; }

        public CommandExecutionArgs(object param)
        {
            ExecutionParam = param;
        }
    }
}
